import Vue from 'vue'
import App from './App'
import apiList from './common/apiList.js'
import {
	ajax
} from 'common/request.js'
import {
	sToast,
	back,
	open,
	showModal
} from 'common/tool.js'
import {
	apikey,avatarUrl
} from 'common/config.js'

Vue.config.productionTip = false
Vue.prototype.$ajax = ajax
Vue.prototype.$back = back
Vue.prototype.$sModal = showModal
Vue.prototype.$sToast = sToast
Vue.prototype.$open = open
Vue.prototype.apiList = apiList
Vue.prototype.apikey = apikey

// import dimg from './components/img/img'

// Vue.component('dimg', dimg)

// 保留两位小数
Vue.filter('toTwo', (value,num) => {
	value=value?value:0
	value=value || 0
	return parseFloat(value).toFixed(num)
})

// 计算几分钟之前
Vue.filter('timediff', begin_time => {
	let end_time = parseInt(new Date().getTime() / 1000);
	//计算天数
	let timediff = end_time - begin_time;
	let days = parseInt(timediff / 86400);
	//计算小时数
	let remain = timediff % 86400;
	let hours = parseInt(remain / 3600);
	//计算分钟数
	remain = remain % 3600;
	let mins = parseInt(remain / 60);
	let res = (days > 0 ? days + '天' : '') + (hours > 0 ? hours + '小时' : '') + mins + '分钟前';
	return mins==0?'刚刚':res;
})
// 时间戳转 mm-dd hh：ss
Vue.filter('time', value => {
	const date = new Date(value * 1000); //时间戳为10位需*1000，时间戳为13位的话不需乘1000

	const M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';

	const D = (date.getDate()  < 10 ? '0' + date.getDate() : date.getDate())  + '  ';

	const h = date.getHours() + ':';

	const m = (date.getMinutes() + 1 < 10 ? '0' + date.getMinutes() : date.getMinutes())

	return M + D + h + m
})

// 时间戳转 yy-mm-dd
Vue.filter('birtime', value => {
	const date = new Date(value * 1000); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
	const Y = date.getFullYear() + '-';
	const M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
	const D = date.getDate()
	return Y + M + D
})

// 头像
Vue.filter('avatar', id => {
	let userid=id || +uni.getStorageSync('userinfo').userId
	return avatarUrl+'avatar/o/'+(parseInt(userid) % 10000)+'/'+userid+'.jpg'+"?x="+Math.random()*10
})

App.mpType = 'app'

const app = new Vue({
	...App
})
app.$mount()
