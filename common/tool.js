// 提示公共类
export function sToast(title, duration = 2000) {
	uni.showToast({
		title,
		icon: 'none',
		duration
	})
}
// 返回上一页公共类
export function back(delta = 1, time = 0) {
	setTimeout(function() {
		uni.navigateBack({
			delta
		})
	}, time)
}
// 打开新页面公共类 0普通跳转  1底部跳转 2关闭当前页面跳转
export function open(id, type = 0, time = 0) {
	setTimeout(function() {
		if (type == 0) {
			uni.navigateTo({
				url: id
			})
		} else if (type == 1) {
			uni.switchTab({
				url: id
			})
		} else {
			uni.reLaunch({
				url: id
			})
		}
	}, time)
};
// 打开新页面公共类 0普通跳转  1底部跳转 2关闭当前页面跳转
export function showModal(content, title = '温馨提示', showCancel = true, confirmText = '确定', confirmColor) {
	return new Promise((resolve, reject) => {
		uni.showModal({
			title,
			content,
			showCancel,
			confirmText,
			confirmColor,
			success(e) {
				if (e.confirm) {
					resolve(true)
				} else {
					resolve(false)
				}
			}

		})
	})
};
