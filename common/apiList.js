export default{
//注册
register:'api/v1.0/api/member/register',
//登录
logins:'api/v1.0/api/member/login',
//短信验证码获取
 sms:'api/v1.0/sms', 
//校验手机号
checkPhone:'api/v1.0/api/member/checkPhone',
//找回密码
Fpassword:'api/v1.0/api/member/updatePwd', 
//保单商品
getList:'api/v1.0/commodity/getList',  
//支付购买报单产品
order:'api/v1.0/platformOrder/pay/order',
//商品详情
getCommodityById:'api/v1.0/commodity/getCommodityById',
//报单详情
orderDetail:'api/v1.0/platformCommodityOrder/orderDetail',
//报单列表
getSysCommodityOrder:'api/v1.0/commodityOrder/getSysCommodityOrder',
//使用加速卡
useQuicken:'api/v1.0/platformCommodityOrder/useQuicken',
//阅读出彩
showOutOrder:'api/v1.0/platformCommodityOrder/showOutOrder',  
//排位
queryRakingOrderByUser:'api/v1.0/platformCommodityOrder/queryRakingOrderByUser',
//出彩
queryShowOutOrders:'api/v1.0/platformCommodityOrder/queryShowOutOrders', 	
//普通商品
getCommodityCountInfo:'api/v1.0/commodity/getCommodityCountInfo',
//充值中心 USDT
price:'api/v1.0/platformOrder/query/price/url',
//充值中心 USDT
recharge:'api/v1.0/platformOrder/pay/recharge',
//查询用户的财务流水表
queryFinanceDetails:'/api/v1.0/member/finance/queryFinanceDetails',	

// 商城分类 一级分类
getFirstLevelMenu:'api/v1.0/storeCategory/getFirstLevelMenu',
// 二级分类
CategoryById:'api/v1.0/commodityCategory/getCommodityCategoryBystoreCategoryId',
// 所有二级分类
getCommodityCategoryCount:'api/v1.0/commodityCategory/getCommodityCategoryCount',
// 商品列表
getCommodityCountInfoByProductCate:'api/v1.0/commodity/getCommodityCountInfoByProductCate',
// 评论
getMemberComment:'api/v1.0/comment/getMemberComment',
//二维码
queryQRCode:'api/v1.0/api/member/queryQRCode',  
// 商城收货地址列表
getAddressByMemberId:'api/v1.0/receivingAddress/getAddressByMemberId',
// 新增地址
addAddress:'api/v1.0/receivingAddress/addAddress',
//删除地址
delAddress:'api/v1.0/receivingAddress/delAddress',
//修改地址
updateAddress:'api/v1.0/receivingAddress/updateAddress',
//我的团队
queryTeamDetail:'api/v1.0/api/member/queryTeamDetail',
//查询站内消息数量
 queryStationMsgCount:'api/v1.0/api/member/queryStationMsgCount',
//查询站内消息列表
queryStationMsg:'api/v1.0/api/member/queryStationMsg',	
//查询站内消息阅读操作
queryStat:'api/v1.0/api/member/queryStationMsgRead', 
//加入购物车
shoppingCart :'api/v1.0/shoppingCart/shoppingCart',
//购物车列表
getPageShoppingCartInfoByUserId:'api/v1.0/shoppingCart/getPageShoppingCartInfoByUserId',
 //购物车数量
updateShoppingCartById:'api/v1.0/shoppingCart/updateShoppingCartById',	
//删除购物车
 del:'api/v1.0/shoppingCart/delShoppingCartByCommdityId', 
//获取默认地址
getDefByMemberId:'api/v1.0/receivingAddress/getDefByMemberId', 
//商铺
Store:'api/v1.0/store/getStoreAllById',
//确认订单
selectShoppingCartById:'api/v1.0/shoppingCart/selectShoppingCartById', 
 //确认订单列表 
 selectShoppingCar:'api/v1.0/shoppingCart/selectShoppingCar',
 //支付
 createOrder:'api/v1.0/goods/pay/createOrder', 
 // 修改个人信息
update:'api/v1.0/api/member/update',
// 商场订单
getOrderUserId:'api/v1.0/commodityOrder/getOrderUserId',

// 取消订单
updateStateByOrder:'api/v1.0/commodityOrder/updateStateByOrder',
//删除记录
delStateByOrder:'api/v1.0/commodityOrder/delStateByOrder',
//更改订单状态
updateStateByOrderNo:'api/v1.0/commodityOrder/updateStateByOrderNo',
//订单详情
getOrderInfoByOrderNumber:'api/v1.0/commodityOrder/getOrderInfoByOrderNumber',
//评论
addMemberComment:'api/v1.0/comment/addMemberComment',
//判断是否评论
getCommentByUserId:'api/v1.0/comment/getCommentByUserId',
//自己的评价列表
getCommentById:'api/v1.0/comment/getCommentById',
//视频
Video:'api/v1.0/video/getVideo', 
//申请售后
addRefund:'api/v1.0/refund/addRefund',
//订单详情
getOrdetNumber:'api/v1.0/refund/getOrdetNumber',
//商家电话
getPhone:'api/v1.0/commodityOrder/getPhone',
//钱包提现
save:'api/v1.0/transferAudit/addCash',
//申请店铺
addStore:'api/v1.0/store/addStore',
//查询开店状态
getStore:'api/v1.0/store/getStore',
//查询已有加速卡和使用的加速卡数量
queryUseAcceleratorCardNum:'api/v1.0/api/member/queryUseAcceleratorCardNum', 
// 查询用户钱包的今日奖励和昨日奖励
queryFinanceSum:'/api/v1.0/member/finance/queryFinanceSum',
//首页公告列表
gonggaogetList:'api/v1.0/notice/getList',
//商铺
getStoreUserId:'api/v1.0/store/getStoreUserId',
//支付密码
zfPassword:'api/v1.0/api/member/updateZfPwd',
//查询提现开关
getSwitch:'api/v1.0/switch/getSwitch',
//提现修改
// updateInfo:'api/v1.0/api/member/updateInfo'
//查询上一级
getById:'/api/v1.0/api/member/getUser',
//限制提现额度
getQuota:'/api/v1.0/withdrawal/getQuota',
//电子协议
dzgetList:'api/v1.0/agreement/getList',
//更改状态
getAll:'api/v1.0/refund/getAll'
}
