import {
	baseUrl,
	uploadUrl
} from './config.js'
import md5 from './md5.min.js'
export function ajax(url, data = {}, method = 'GET', header = {
	'content-type': 'application/x-www-form-urlencoded',
	'Authorization': 'Bearer ' + uni.getStorageSync('userinfo').token
}, type = 'base') {
	return new Promise((resolve, reject) => {
		uni.showLoading({
			mask: true
		})
		let time = parseInt(new Date().getTime() / 1000)
		if (data.secret == undefined) {
			data.secret = md5(this.apikey + time + uni.getStorageSync('userinfo').userId + uni.getStorageSync('userinfo').token)
		}
		data.time = time
		data.accesstoken = ''
		if (uni.getStorageSync('userinfo') != '') {
			data.accesstoken = uni.getStorageSync('userinfo').token;
		}
		uni.request({
			url: (type == 'base' ? baseUrl : uploadUrl) + url,
			data: data,
			method: method,
			header: header,
			success(res) {
				uni.hideLoading()
				uni.stopPullDownRefresh()
				//resolve(res.data);
				if (res.data.code != 500) {
					resolve(res.data);
				} 
				else {
					//token过期
					if (res.data.msg == '访问令牌过期或无效' || res.data.msg == '缺少访问令牌') {
						uni.showToast({
							title: '您的登陆已过期',
							icon: 'none',
							duration: 1800
						})
						uni.clearStorageSync()
						setTimeout(function() {
							uni.reLaunch({
								url: '/pages/login/login'
							})
						}, 1500)
						return
					}else if(res.data.msg == '您的账号已在另一台设备上登陆'){
						uni.showToast({
							title: '您的账号已在另一台设备上登陆',
							icon: 'none',
							duration: 1800
						})
						uni.clearStorageSync()
						setTimeout(function() {
							uni.reLaunch({
								url: '/pages/login/login'
							})
						}, 1500)
						return
					}
					uni.showToast({
						title: res.data.code || '连接服务器失败,请稍后重试!',
						icon: 'none',
						duration: 1500
					})
					reject(res.data)
				}
			},
			fail(e) {
				uni.showToast({
					title: '连接失败,请检查网络后再试!',
					icon: 'none'
				})
				uni.hideLoading()
			}
		})
	})
}
