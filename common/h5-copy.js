export default function h5Copy(content) {
	// #ifdef H5
	if (!document.queryCommandSupported('copy')) {
		// 不支持
		uni.showToast({
			title: '浏览器暂不支持复制',
			icon: 'none'
		})
		return
	}

	let textarea = document.createElement("textarea")
	textarea.value = content
	textarea.readOnly = "readOnly"
	document.body.appendChild(textarea)
	textarea.select() // 选择对象
	textarea.setSelectionRange(0, content.length) //核心
	let result = document.execCommand("copy") // 执行浏览器复制命令
	textarea.remove()
	uni.showToast({
		title: '复制成功'
	})
	return result
	// #endif
	// #ifndef H5
	uni.setClipboardData({
		data: JSON.stringify(content)
	})
	// #endif

}
